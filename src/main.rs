use std::io::{Read};
use std::fs::File;
// use tokio::net::windows::named_pipe::{ClientOptions};
use reqwest::Client;
use serde::Deserialize;
use serde_json;

#[derive(Deserialize)]
struct TranslationResponse {
    translations: Vec<Translation>,
}

#[derive(Deserialize)]
struct Translation {
    text: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    //
    //let pipe_name = r"\\.\pipe\MyPipe";
    //let named_pipe_client = ClientOptions::new().open(pipe_name)?;
    //

    let mut log_file = File::open(r"C:\Users\Anwender\AppData\Roaming\DDNet\logfile")?;
    let mut log_contents = String::new();
    log_file.read_to_string(&mut log_contents)?;

    let lines: Vec<String> = log_contents.lines().map(|line| line.to_string()).collect();

    if let Some(last_line) = lines.last() {
        println!("Full Line: {}", last_line);

        let parts: Vec<&str> = last_line.split(':').collect();
        if parts.len() >=   3 {
            let message_to_translate = parts[2].trim();

            let http_client = Client::new();
            let response = http_client.post("https://api-free.deepl.com/v2/translate")
                .query(&[
                    ("auth_key", ""),
                    ("text", message_to_translate),
                    ("target_lang", "DE"),
                ])
                .send()
                .await?;

            let response_body = response.text().await?;
            println!("Raw Response: {}", response_body);

            let response: Result<TranslationResponse, _> = serde_json::from_str(&response_body);
            match response {
                Ok(data) => {
                    let translated_text = data.translations.get(0).map(|t| t.text.clone()).unwrap_or_default();
                    println!("Translated message: {}", translated_text);
                },
                Err(e) => eprintln!("Failed to parse JSON: {}", e),
            }
        } else {
            eprintln!("Could not parse the last message");
        }
    } else {
        eprintln!("The log file is empty");
    }

    Ok(())
}
